package com.cineast.bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pidev.persistence.Movies;
import com.pidev.res.MoviesResLocal;

@ManagedBean
@ViewScoped
public class MoviesBean {
	
	@EJB 
	private MoviesResLocal service;
	
	private List<Movies> movies;
	private Movies movie;
	private Movies newMovie;
	private boolean formDisplayed;
	private String videoUrl;
	


	public MoviesBean() {
		
	}
	@PostConstruct
	private void initModel(){
		formDisplayed=false;
		movies=service.getAll();
		movie=new Movies();
		videoUrl= "http://localhost:2000/Video/"+movies.get(0).getTrailer()+".mp4";
		
	}
	
	public void onRowSelect(){
		
		System.out.println(videoUrl);
		System.out.println(movie);
		videoUrl= "http://localhost:2000/Video/"+movie.getTrailer()+".mp4";
		 		
	}
	
	public void doAdd(){
		System.out.println("doadd");
		
		formDisplayed=true;
		
	}
	
	public void doDelete(){
		
		for (Movies m : movies) {
			if(m.getId()==movie.getId()){
				movie=m;	
				
			break;
			}		
		}
		System.out.println("toDelete:"+movie);
		service.delete(movie);
		movies=service.getAll();
		
	}
	
	public void doSave(){
		
		System.out.println("doSave");
		service.create(newMovie);
	}
	
	public void doCancel(){
		System.out.println("doCancel");
		movie=new Movies();
		formDisplayed=false;
	}
	
	
	
	//get& set**********************************************
	public List<Movies> getMovies() {
		return movies;
	}

	public void setMovies(List<Movies> movies) {
		this.movies = movies;
	}

	public Movies getMovie() {
		return movie;
	}

	public void setMovie(Movies movie) {
		this.movie = movie;
	}
	public boolean isFormDisplayed() {
		return formDisplayed;
	}
	public void setFormDisplayed(boolean formDisplayed) {
		this.formDisplayed = formDisplayed;
	}
	public String getVideoUrl() {
		return videoUrl;
	}
	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}
	public Movies getNewMovie() {
		return newMovie;
	}
	public void setNewMovie(Movies newMovie) {
		this.newMovie = newMovie;
	}

	
	

}
