package com.pidev.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.pidev.mbeans.HelperBean;
import com.pidev.persistence.Movies;

@FacesConverter("toMovie")
public class CinemaTheaterConverter implements Converter{
	
	

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Movies equivalentMovie = null;
		if (!value.equals("")) {
			HelperBean helperBean = context
						.getApplication()
							.evaluateExpressionGet(context, "#{helperBean}", HelperBean.class);
			equivalentMovie = helperBean.findMovieByName(value);
		}
		return equivalentMovie;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		String equivalentString = null;
		if (value == null || value.equals("")) {
			equivalentString = "";
		}else{
			equivalentString = ((Movies)value).getTitle();
		}
		return equivalentString;
	}

}
