package com.pidev.mbeans;



import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;



@ManagedBean(name="authBean")
@SessionScoped
public class AutenticationBean {
	private String login, password;
	private boolean loggedIn;
	
	public AutenticationBean() {
	}
	
	public String doLogin() {
		String navigateTo = null ;	
		if (login.equals("admin") && password.equals("root")) {
			setLoggedIn(true);
			navigateTo = "/pages/admin/home?faces-redirect=true";
		} else {
			//navigateTo = "/pages/erreur?faces-redirect=true";
			FacesContext
			.getCurrentInstance()
			.addMessage("login_form:login_submit", new FacesMessage("Bad credentials!"));
		}
		return navigateTo;
	}
	

	public String doLogout(){
		String navigateTo = null;
		//initModel();
		FacesContext
			.getCurrentInstance()
			.getExternalContext()
			.getSessionMap()
			.clear();
		navigateTo = "/welcome?faces-redirect=true";	
		return navigateTo;
	}
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}


	
	
}
