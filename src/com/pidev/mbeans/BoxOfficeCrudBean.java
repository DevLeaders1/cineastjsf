package com.pidev.mbeans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.SelectEvent;

import com.pidev.ejb.GenericCrudServiceBeanLocal;
import com.pidev.persistence.BoxOffice;
import com.pidev.persistence.Movies;

/**
 * @author Marwen
 *
 */

@ManagedBean
@ViewScoped
public class BoxOfficeCrudBean {
	
	@EJB
	private GenericCrudServiceBeanLocal beanLocal;
	
	private BoxOffice boxOffice;
	private List<BoxOffice> boxOffices;
	private List<BoxOffice> filtredBoxOffice;
	private List<Movies> movies;
	/**
	 * 
	 */
	public BoxOfficeCrudBean() {}

	@PostConstruct
	public void initModel(){
		boxOffice = new BoxOffice();
		boxOffices = beanLocal.findWithNativeQuery("select * from BoxOffice", BoxOffice.class);
		movies = beanLocal.findWithNativeQuery("select * from Movies", Movies.class);
	}
	
	public void doSave(){			
		beanLocal.update(boxOffice);
	 	initModel();		
	}

	public void doDelete(){
		beanLocal.delete(boxOffice);
		initModel();		
	}
	
	public void doCancel(){
		boxOffice = new BoxOffice();
	}
	public void onRowSelect(SelectEvent event){	
	}
	
	public void onFilter(SelectEvent event){
	}

	public BoxOffice getBoxOffice() {
		return boxOffice;
	}

	public void setBoxOffice(BoxOffice boxOffice) {
		this.boxOffice = boxOffice;
	}

	public List<BoxOffice> getBoxOffices() {
		return boxOffices;
	}

	public void setBoxOffices(List<BoxOffice> boxOffices) {
		this.boxOffices = boxOffices;
	}

	public List<BoxOffice> getFiltredBoxOffice() {
		return filtredBoxOffice;
	}

	public void setFiltredBoxOffice(List<BoxOffice> filtredBoxOffice) {
		this.filtredBoxOffice = filtredBoxOffice;
	}
		

}
