package com.pidev.mbeans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.pidev.ejb.GenericCrudServiceBeanLocal;
import com.pidev.persistence.CinemaTheatres;
import com.pidev.persistence.Movies;

/**
 * @author Marwen
 *
 */

@ManagedBean
@ViewScoped
public class CinemaTheaterCrudBean {
	
	@EJB
	private GenericCrudServiceBeanLocal beanLocal;
	
	private CinemaTheatres cinemaTheatre ;
	private List<CinemaTheatres> cinemaTheatres;
	private List<CinemaTheatres> filtredCinema;
	private List<Movies> movies;
	private boolean formDisplayed;
	
	/**
	 * 
	 */
	public CinemaTheaterCrudBean() {}

	@PostConstruct
	public void initModel(){
		formDisplayed = false;
		cinemaTheatre = new CinemaTheatres();
		cinemaTheatres = beanLocal.findWithNativeQuery("select * from CinemaTheatres", CinemaTheatres.class);
		movies = beanLocal.findWithNativeQuery("select * from Movies", Movies.class);
	}
	
	public void doSave(){			
		formDisplayed = true;
		beanLocal.update(cinemaTheatre);
		cinemaTheatres = beanLocal.findWithNativeQuery("select * from CinemaTheatres", CinemaTheatres.class);
	}

	public void doDelete(){
		formDisplayed = true;
		beanLocal.delete(cinemaTheatre);
		cinemaTheatres = beanLocal.findWithNativeQuery("select * from CinemaTheatres", CinemaTheatres.class);
		
	}
	
	public void doCancel(){
		formDisplayed = false;
	}
	
	public void doNew(){
		cinemaTheatre = new CinemaTheatres();
		formDisplayed = true;
	}
	
	public void onRowSelect(){	
		formDisplayed = true;
	}
	
	public void onFilter(){
		formDisplayed = false;
	}
	
	public CinemaTheatres getCinemaTheatre() {
		return cinemaTheatre;
	}

	public void setCinemaTheatre(CinemaTheatres cinemaTheatre) {
		this.cinemaTheatre = cinemaTheatre;
	}

	public List<CinemaTheatres> getCinemaTheatres() {
		return cinemaTheatres;
	}

	public void setCinemaTheatres(List<CinemaTheatres> cinemaTheatres) {
		this.cinemaTheatres = cinemaTheatres;
	}

	public List<Movies> getMovies() {
		return movies;
	}

	public void setMovies(List<Movies> movies) {
		this.movies = movies;
	}

	public List<CinemaTheatres> getFiltredCinema() {
		return filtredCinema;
	}

	public void setFiltredCinema(List<CinemaTheatres> filtredCinema) {
		this.filtredCinema = filtredCinema;
	}

	public boolean isFormDisplayed() {
		return formDisplayed;
	}

	public void setFormDisplayed(boolean formDisplayed) {
		this.formDisplayed = formDisplayed;
	}


}
