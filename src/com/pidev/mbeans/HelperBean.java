package com.pidev.mbeans;

import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import com.pidev.ejb.GenericCrudServiceBeanLocal;
import com.pidev.persistence.Movies;


@ManagedBean
@ApplicationScoped
public class HelperBean {
	
	
	@EJB
	private GenericCrudServiceBeanLocal beanLocal;
	
	
	public HelperBean() {
	}


	public Movies findMovieByName(String name) {
		//System.out.println(beanLocal.findMoviesByName(name));
		return beanLocal.findMoviesByName(name);
	}


	
	
	

}
